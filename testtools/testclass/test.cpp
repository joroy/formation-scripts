#include "gtest/gtest.h"
#include "gmock/gmock.h"

namespace { 

class %ClassName% : public ::testing::Test { 
protected: 
   %ClassName%() 
   {
      //Setup test (Before every test)
   }

   virtual ~%ClassName%()
   {
      //Teardown test (After every test)
   }

   // Objects declared here can be used by all tests in the test case for %ClassName%. 
}; 

TEST_F(%ClassName%, GivenX_WhenY_ThenZ) {
   
}

}  // namespace 
