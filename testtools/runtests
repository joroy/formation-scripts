#!/bin/bash

#before everything, if there's a script of the same name in the currect directory, run it instead
SCRIPT_PATH="$( dirname "${BASH_SOURCE[0]}" )"
CURRENT_PATH="$( pwd )"
if [[ "$SCRIPT_PATH" != "$CURRENT_PATH" ]] && [ -f "$(readlink -f $(basename $0))" ]; then
    $(basename $0) $@
    exit $?
fi


function usage(){
    echo -e "Usage: `basename $0`"
    echo -e "       Tries to find every test projects/scripts and runs them."
    echo -e "Usage: `basename $0` project_tests.[pro|sh]"
    echo -e "       Tries to run tests for the provided project/script."
    echo -e "       If the argument is not a valid relative path,"
    echo -e "       `basename $0` tries to find it in the whole git." 
    echo -e "Usage: `basename $0` -b builddir [project_tests.pro|git]"
    echo -e "       Changes the default build directory to builddir."

}

REGEX_TESTS_PROJECTS='.*\_[Uu]nit[Tt]est[s]?\.pro'
REGEX_TESTS_SCRIPTS='.*\_[Uu]nit[Tt]est[s]?\.sh'
BUILDS=$HOME/builds
VERBOSE=""
while getopts ":av,:b:,:h,:v" opt; do
    case $opt in
        a)
            REGEX_TESTS_PROJECTS='.*\_\([Uu]nit\)?[Tt]est[s]?\.pro'
            REGEX_TESTS_SCRIPTS='.*\_\([Uu]nit\)?[Tt]est[s]?\.sh'
            ;;
        b)
            BUILDS=$(readlink -f $OPTARG)
            ;;
        h)
            usage
            exit 0
            ;;
        v)
            VERBOSE="-v"
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            usage
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            usage
            exit 1
            ;;
    esac
done
shift $(($OPTIND-1))

TOPGITDIR=$(git rev-parse --show-toplevel)
if [ -z $TOPGITDIR ]; then
    exit 1
fi

# Retrieve projects and scripts to run
if [ $# -eq 1 ]; then
    PROJECTS=$(readlink -f $1)
    if [ ! -f "$PROJECTS" ]; then
        PROJECTS=$(find $TOPGITDIR -type f -name "`basename $PROJECTS`" -exec readlink -f {} \;)
    fi

    if [[ $PROJECTS != *".pro" ]]; then
        SCRIPTS="$PROJECTS"
        PROJECTS=""
    fi

else
    PROJECTS=$(find $TOPGITDIR -type f -regex ${REGEX_TESTS_PROJECTS})
    SCRIPTS=$(find $TOPGITDIR -type f -regex ${REGEX_TESTS_SCRIPTS})
fi

for PROJECT in $PROJECTS
do
    projectName=$(basename $PROJECT | sed 's/\.pro//g')
    buildir=$(echo "build-$projectName-runtests")

    if [ ! -d "$BUILDS/$buildir" ]; then
        mkdir -p $BUILDS/$buildir
    fi

    cd $BUILDS/$buildir

    set -e
    echo -n "$projectName: qmake..."
    qmake -r $PROJECT > /dev/null
    echo -n "clean..."
    make clean > /dev/null
    echo -n "make..."
    make -j8 > /dev/null
    set +e

    EXENAMES=$(find . -executable -type f  | grep -v "/\.debug/")

    if [ -z "$EXENAMES" ]; then
        echo "FAILED!! No target was found."
        exit 1
    fi

    echo -n "run..."
    for EXE in $EXENAMES; do
        echo -n "$(basename $EXE)..."
        result=$($EXE)
        EXITCODE=$?
        if [ "$EXITCODE" != "0" ]; then
            echo
            echo "$result"
            hasFailedTests=$(echo $result | grep "FAILED")
            hasPassedTests=$(echo $result | grep "PASSED")
            if [ -n "$hasFailedTests" ]; then
                echo "`basename $EXE` FAILED!!"
                exit 1
            elif [ -z "$hasPassedTests" ]; then
                echo "`basename $EXE` CRASHED!!"
                exit 1
            fi
        fi
    done
    echo "Success!"
done

cd $TOPGITDIR
for SCRIPT in $SCRIPTS
do
    echo -n "$(basename $SCRIPT): run..."
    $SCRIPT $BUILDS $VERBOSE 
    if [[ "$?" != "0" ]]; then
        echo "FAILED!!"
        exit 1
    else
        echo "Success!"
    fi
done
exit 0
