#!/usr/bin/env bash
# The script will search in every cpp files, which are potentially test files,
# to find inclusion of mocks. When one is found, this will trigger the mock's
# header generation.
SOURCES_DIR=$1
SOURCE_FILES=$(find $SOURCES_DIR -name *.cpp)
HEADERS_FILES=$(find $SOURCES_DIR -name *.h*)

RegularIFS=$IFS
NewLineIFS=$(echo -en "\n\b")

# Scan in every source files
for sourceFile in $SOURCE_FILES; do
	dirName=$(dirname $sourceFile)

	# Every include line ending with "Mock.h" is the keyword for a mock inclusion.
	# Note that this script currently does not support hpp headers.
	# Each matching line will be parsed to extract the expected file name of the
	# desired header to mock.
	IFS=$NewLineIFS
	for newMock in $(grep "#include.*Mock.h" $sourceFile); do
		newMock="$(echo $newMock | awk -F\" '{print $2}' | sed 's/\.h$//')"

		INTERFACE_TO_MOCK="${newMock%Mock}.h"
		echo "INTERFACE_TO_MOCK = $INTERFACE_TO_MOCK"

		# Scan through each header file to detect a match with the desired mock.
		IFS=$RegularIFS
		for header in $HEADERS_FILES; do
			if [ "$(basename $INTERFACE_TO_MOCK)" == "$(basename $header)" ]; then
				# There is a match. Call the mock generator with that file.
				mmock "$header" "$dirName" "$newMock.h"
			fi
		done
	done
	IFS=$RegularIFS
done

